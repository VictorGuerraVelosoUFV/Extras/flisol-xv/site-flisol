import React from 'react';

export function CardSlot(props){
    return (
        <div className="d-md-flex flex-row">
            {props.children}
        </div>
    );
};