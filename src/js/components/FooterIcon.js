import React from 'react';
import '../../css/FooterIcon.css'

export function FooterIcon(props) {
    let styles = "footer-icon"
    return(
        <div>
            <a className="tooltip-container" href={props.url}>
                <span className="tooltiptext">{props.text}</span>
                <img src={props.img} alt="Ícone do rodapé" className={styles}></img>
            </a>
            {props.children}
        </div>
    )
}