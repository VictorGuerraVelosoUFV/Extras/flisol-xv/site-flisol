import React, { Component } from 'react';
import { connect } from 'react-redux';
import { closeModal } from "../actions/HeaderActions";
import { StockModal } from './StockModal';
import { FormInputItem, FormTextInputMaskItem } from './FormItem';
class Register extends Component{
  render(){
    return (
      <StockModal default title="Cadastro" opened={this.props.modal_type === "Cadastro"}>
        <FormInputItem id="RG" label="RG" placeholder="Escreva seu RG" />
        <FormTextInputMaskItem id="CPF" label="CPF" type="cpf" placeholder="Escreva seu CPF" />
        <FormInputItem type="password" id="password" label="Senha" placeholder="Escreva sua senha" />
      </StockModal>
    );
  }
};
const mapDispatchToProps = {
    closeModal
}
function mapStateToProps(state){
    return {
        modal_type: state.header.modal_type
    };
};

Register = connect(mapStateToProps,mapDispatchToProps)(Register);
export {Register};
