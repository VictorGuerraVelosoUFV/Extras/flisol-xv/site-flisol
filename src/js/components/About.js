import React, {Component} from 'react';
import "../../css/Attraction.css";
import { CardSlot } from "./CardSlot";
import { StockCard } from "./StockCard";
import { CardModel } from "../models/CardModel";
import cefet_banner from "../../assets/cefet-banner.jpg";
import flisol_banner from "../../assets/flisol-banner.jpg";
import compet_banner from "../../assets/compet-banner.png";

class About extends Component{
    render(){
        return (
            <div>
                <CardSlot>
                    <StockCard card={
                        new CardModel(
                            flisol_banner,
                            <h3>A FLISoL</h3>, 
                            "Festival Latino-americano de Instalação de Software Livre", 
                            (<span>O Festival Latino-americano de Instalação de Software Livre (FLISoL) é o maior evento da América Latina de divulgação de Software Livre. Ele é realizado desde o ano de 2005, e desde 2008 sua realização acontece no 4º sábado de abril de cada ano. Seu principal objetivo é promover o uso de Software Livre, mostrando ao público em geral sua filosofia, abrangência, avanços e desenvolvimento.                
                            <br/>Para alcançar este objetivo, diversas comunidades locais de Software Livre (em cada país/cidade/localidade), organizam simultaneamente eventos em que se instala, de maneira gratuita e totalmente legal, softwares e sistemas operacionais livres nos computadores dos participantes. Além disso, paralelamente acontecem palestras, apresentações e workshops, sobre temas locais, nacionais e latino-americanos sobre Software Livre, em toda a sua expressão: artística, acadêmica, empresarial e social.</span>)
                            )}>
                    </StockCard>
                </CardSlot>
                <CardSlot>
                    <StockCard card={
                        new CardModel(
                            cefet_banner,
                            <h3>O CEFET</h3>, 
                            "Festival Latino-americano de Instalação de Software Livre", 
                            (<span>O Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG) é uma autarquia federal brasileira, vinculada ao Ministério da Educação, que oferece ensino médio, cursos técnicos, superiores, pós stricto sensu e lato sensu, contemplando também, de forma não dissociada, o ensino, a pesquisa e a extensão, na área tecnológica e no âmbito da pesquisa aplicada. O CEFET-MG oferece ao seu aluno uma formação acadêmica completa, desde o técnico de nível médio até o doutoramento. Dentro da Instituição, estudantes de todos os níveis integram grupos de pesquisas, compartilham conhecimento e são orientados por um corpo docente apto e atuante em todas as camadas de ensino. O reconhecimento desse empenho do CEFET-MG é visto tamanho o investimento do Governo Federal e do Governo Estadual em bolsas para seus pesquisadores.</span>)
                            )}>
                    </StockCard>
                </CardSlot>
                <CardSlot>
                    <StockCard card={
                        new CardModel(
                            compet_banner,
                            <h3>O COMPET</h3>, 
                            "Festival Latino-americano de Instalação de Software Livre", 
                            (<span>Os Programas de Educação Tutorial (PET’s) são desenvolvidos por grupos de estudantes, com tutoria de um docente, organizados a partir de formações em nível de graduação nas Instituições de Ensino Superior do País orientados pelo princípio da indissociabilidade entre ensino, pesquisa e extensão e da educação tutorial. No CEFET-MG, o PET da graduação de Engenharia de Computação é conhecido por todos como COMPET. Assim, este procura aproximar os petianos do mercado de trabalho, desenvolvendo projetos em diversas áreas como robótica e web.</span>)
                            )}>
                    </StockCard>
                </CardSlot>
            </div>
            
        )
    }
}

export {About};