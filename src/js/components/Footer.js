import React from 'react';
import { FooterIcon } from './FooterIcon';
import eletronite from "../../assets/logo-eletronite.png";
import cefet from "../../assets/logo-cefet.jpg";
import compet from "../../assets/logo-compet.png";
import enxurrada from "../../assets/logo-enxurrada.png";
import pacific from "../../assets/logo-pacific.png";
import trinca from "../../assets/logo-trinca.png";
import ufv from "../../assets/logo-ufv.png";
import "../../css/Rodape.css";

export function Footer(props) {
    return (
        <div className="Rodape d-block d-md-flex" color="light" expand="md">
            <div className="col">
                <ul>
                    <h4>Patrocinadores</h4>
                    <FooterIcon img={eletronite} url="https://www.eletronite.com.br/" text="Eletronite - Eletrônica, tecnologia e inovação" />
                </ul>
            </div>
            <div className="col">
                <ul className="d-flex flex-column">
                    <h4>Apoiadores</h4>
                    <FooterIcon img={ufv} url="https://www.eletronite.com.br/" text="Universidade Federal de Viçosa Campus Florestal" />
                    <FooterIcon img={trinca} url="http://trincabotz.com.br/" text="Trincabotz - Equipe de Robótica Aplicada a Competição" />
                    <FooterIcon img={enxurrada} url="https://sites.google.com/view/enxurradadebits/" text="Enxurrada de Bits" />
                    <FooterIcon img={pacific} url="https://www.facebook.com/pacificbotz/" text="Pacific Botz" />
                </ul>
            </div>
            <div className="col">
                <ul>
                    <h4>Realização</h4>
                    <FooterIcon img={cefet} url="https://www.cefetmg.br/" text="Centro Federal de Educação Tecnológica de Minas Gerais" />
                    <FooterIcon img={compet} url="http://compet.decom.cefetmg.br/" text="Programa de Educação Tutorial do Curso de Engenharia de Computação" />
                </ul>
            </div>
        </div>
    )
};