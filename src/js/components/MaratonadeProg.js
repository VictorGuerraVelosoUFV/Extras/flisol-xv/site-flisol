import React, { Component } from "react";
import "../../css/Attraction.css";
import { CardSlot } from "./CardSlot";
import { StockCard } from "./StockCard";
import { CardModel } from "../models/CardModel";
import { Button, FormGroup, Label } from "reactstrap";
import maratona_banner from "../../assets/maratona-banner.jpg";
import { StockModal } from "./StockModal";
import { connect } from 'react-redux';
import { setModalType, openModal } from "../actions/HeaderActions";
import { FormInputItem, FormTextInputMaskItem, FormRadioItem } from "./FormItem";
class MaratonaDeProgramacao extends Component{
    maratonaDeProgramacaoContent(){
        return (
                <span>
                A Maratona de Programação do Flisol é uma competição, com duração de 2 horas, na qual equipes de três competidores devem resolver, de forma correta e eficiente, um caderno com um conjunto de problemas usando alguma linguagem de programação entre C, C++, Java e Python.<br/>
                Cada problema possui um enunciado a qual as equipes devem interpretar, modelar e implementar uma solução utilizando uma ou mais técnicas de programação que pode envolver processamento de cadeias de caracteres, grafos, paradigmas, matemática, estruturas de dados ou algum algoritmo específico. Ademais, cada solução deve respeitar um padrão de entrada e saída de dados especificado pela questão.<br/>
                Para avaliar as soluções elaboradas será utilizado o software juiz online "BOCA Online Contest Administrator", que é <a href="https://www.ime.usp.br/~cassio/boca/">disponibilizado livremente</a>.<br/>
                Os enunciados das questões, bem como as entradas e saídas oficiais que serão usadas na avaliação das soluções submetidas foram formuladas pelo professor André Rodrigues da Cruz do CEFET-MG. O servidor Boca que será usado na competição será configurado pelo professor Andrei Rimsa Álvares, também do CEFET-MG.
                </span>
        )
    }
    render() {
        return (
        <div>
            <CardSlot>
                <StockCard card={
                    new CardModel(
                        maratona_banner,
                        "Maratona de Programação", 
                        "10h-13h - Laboratório CCC", 
                        this.maratonaDeProgramacaoContent()
                        )}>
                            <Button onClick={()=>{this.props.setModalType("Maratona de Programação");this.props.openModal();}}> Inscreva sua equipe! </Button>
                        </StockCard>
            </CardSlot>
            <StockModal title="Maratona de Programação" opened={this.props.modal_type === "Maratona de Programação"}>
                <FormInputItem id="name" label="Nome Completo" placeholder="Escreva seu nome" />
                <FormTextInputMaskItem id="CPF" label="CPF" type="cpf" placeholder="Escreva seu CPF" />
                <FormInputItem type="email" id="email" label="Email" placeholder="Escreva seu e-mail" />

                <FormGroup tag="fieldset">
                    <Label>Linguagem de programação:</Label>
                    <FormRadioItem name="teamlang" id="C" />
                    <FormRadioItem name="teamlang" id="C++" />
                    <FormRadioItem name="teamlang" id="Java" />
                    <FormRadioItem name="teamlang" id="Python" />
                </FormGroup>

                <FormInputItem id="teammate1name" label="2º membro da equipe" placeholder="Escreva o nome do seu colega de equipe" />
                <FormTextInputMaskItem id="teammate1CPF" label="CPF" type="cpf" placeholder="Escreva o CPF do seu colega de equipe" />
                <FormInputItem type="email" id="teammate1email" label="Email" placeholder="Escreva o e-mail do seu colega de equipe" />
                
                <FormInputItem id="teamname2name" label="3º membro da equipe" placeholder="Escreva o nome do seu colega de equipe" />
                <FormTextInputMaskItem id="teamname2CPF" label="CPF" type="cpf" placeholder="Escreva o CPF do seu colega de equipe" />
                <FormInputItem type="email" id="teamname2email" label="Email" placeholder="Escreva o e-mail do seu colega de equipe" />
            </StockModal>
        </div>)

    }
};
function mapStateToProps(state){
    return {
        modal_type: state.header.modal_type
    }
}
const mapDispatchToProps = {
    setModalType,
    openModal
}
MaratonaDeProgramacao = connect(mapStateToProps,mapDispatchToProps)(MaratonaDeProgramacao);
export {MaratonaDeProgramacao};