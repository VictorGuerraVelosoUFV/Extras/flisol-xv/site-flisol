import React, {Component} from 'react';
import "../../css/Attraction.css";
import { CardSlot } from "./CardSlot";
import { StockCard } from "./StockCard";
import { CardModel } from "../models/CardModel";
import { Input, Button } from "reactstrap";
import camisa_banner from "../../assets/Camisa.jpg";
import { connect } from "react-redux"
import { onItemValueChanged } from '../actions/FormActions';
import { getSubscriptions } from '../actions/ContentActions';
import { Redirect } from "react-router-dom"

class MeuPerfil extends Component{
    selectShirtSize(event){
        this.props.onItemValueChanged("shirt_size",event.target.value);
    }
    cardContent(){
        return (
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_s-xclick" />
                <input type="hidden" name="on0" value="Tamanhos" />
                Escolha o tamanho:<br/>
                <Input defaultValue={this.props.shirt_size} onChange={this.selectShirtSize.bind(this)} type="select" name="os0">
                    <option value="PP">PP R$25,00</option>
                    <option value="P">P R$25,00</option>
                    <option value="M">M R$25,00</option>
                    <option value="G">G R$25,00</option>
                    <option value="GG">GG R$25,00</option>
                </Input><br/>
                <input type="hidden" name="currency_code" value="BRL" />
                <input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIIaQYJKoZIhvcNAQcEoIIIWjCCCFYCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYAyW0NGC3rPx4orguOXfzjn6BPfhaSqZDCQ1s8BG+ZIDk3sSM5jJNjVunG4enXSfm5EDBPDFRXNZee1rFUwPyxer23xHp5d1vr+o7B4SPrF37305as0bAborSb1AMkv60Gy8abmtNu1YiuEQLQ3nertG6n8SGWsmLhsTQd6dKR+vDELMAkGBSsOAwIaBQAwggHlBgkqhkiG9w0BBwEwFAYIKoZIhvcNAwcECGVbo5lLAXhagIIBwOk6RMWK1t/v0TzrhFV/ZTpkASPVy8owPJz5Xr1KiM/UketWDl2p9QlJsONlIQupp7/gkgCr94aEusSfiUp/jMlkto5xScqwlzT2QhAzChwaxPAE/HxfHI5pxV76hP+caizp+XeSPX3LndtyxoK82mc9q3qLBvtetROzITALLnfk27v3x1+KTo5NdL6h+3n7/R1SXsuHXHbd5hsHpR5lBJ637y6IBcfq1bOEvEEcuCfdybsQMFcIrF0unXsWPIKzvXcwHmTmPFd4nl6mqOd7Dtwtoks3LcLAwAirJoM5r1G5LtMSDBtMi1yAG8upp+KoyIX/yMtU1HKBD7n6FDCB9X+zyd4u+te97ogW7eH8LeVmPTITMpe54h6Emf6eqbhfDzbiqkxExPLeATLXdJDxUH+5cq5pvdfj6XXjdDRhnx/RCZYsV5CW1HBYtQ7qgKTOz+RHVh/vAd8FAoCaai5tU1zB8D4tCJbF/YiqAkCrFNOqFxEfw0WRLRcPyUn49vlOqi6dnV2pgtENuOxCXOD9SmIQi8A+Y0v3F+eq2FaOFuVxufwP5BnZcC7GPjhwHVVOdlVRCsNljpbbt3Flo+KhYVqgggOHMIIDgzCCAuygAwIBAgIBADANBgkqhkiG9w0BAQUFADCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wHhcNMDQwMjEzMTAxMzE1WhcNMzUwMjEzMTAxMzE1WjCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMFHTt38RMxLXJyO2SmS+Ndl72T7oKJ4u4uw+6awntALWh03PewmIJuzbALScsTS4sZoS1fKciBGoh11gIfHzylvkdNe/hJl66/RGqrj5rFb08sAABNTzDTiqqNpJeBsYs/c2aiGozptX2RlnBktH+SUNpAajW724Nv2Wvhif6sFAgMBAAGjge4wgeswHQYDVR0OBBYEFJaffLvGbxe9WT9S1wob7BDWZJRrMIG7BgNVHSMEgbMwgbCAFJaffLvGbxe9WT9S1wob7BDWZJRroYGUpIGRMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbYIBADAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBAIFfOlaagFrl71+jq6OKidbWFSE+Q4FqROvdgIONth+8kSK//Y/4ihuE4Ymvzn5ceE3S/iBSQQMjyvb+s2TWbQYDwcp129OPIbD9epdr4tJOUNiSojw7BHwYRiPh58S1xGlFgHFXwrEBb3dgNbMUa+u4qectsMAXpVHnD9wIyfmHMYIBmjCCAZYCAQEwgZQwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tAgEAMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xOTAzMjgwMjA1MjJaMCMGCSqGSIb3DQEJBDEWBBQUW7ZJwDAVB+r3H5ATX/UbTiyTDjANBgkqhkiG9w0BAQEFAASBgBJJlHOk9RTc6L1vnHPw0xh4FoLXBKyi5i8B+m3h6t8y7enmf8B5PMfD5UxBjiSCHBC554qUjAP1JmYojH0VOXp+EEIyPlOwGeqF0mdBOKbfoOnDltt8NYOQiAzYBscct3SelxToD4SOFmFjDmXiMufsL8/eaec2EyFs8TuCIQBk-----END PKCS7-----" />
                <input type="image" src="https://www.paypalobjects.com/pt_BR/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal - A maneira fácil e segura de enviar pagamentos online!" />
                <img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1" />
            </form>
        );
    }
    componentDidMount(){
        this.props.getSubscriptions(this.props.user_id,this.props.token);
    }
    render(){

        return (
            <div>
                {this.props.logged_in?[]:<Redirect to='/Home' />}
                <CardSlot>
                    <StockCard card={
                        new CardModel(
                            camisa_banner,
                            <h3>COMPRE UMA CAMISA EXCLUSIVA DO EVENTO</h3>, 
                            "Atenção: neste ano a camisa eleita é a azul com laranja.", 
                            this.cardContent())}>
                    </StockCard>
                </CardSlot>
                { this.props.subscriptions.map((card,index)=>{
                    return(
                        <CardSlot key={index}>
                                <StockCard key={index} card={card}>
                                    <Button onClick={()=>this.props.subscribeToActivity(card.id)}> {card.button_text} </Button>
                                </StockCard>
                        </CardSlot>
                        )
                    })}
            </div>
            
        )
    }
}
function mapStateToProps(state){
    window.state = state;
    return {
        shirt_size: state.header.form_inputs.shirt_size,
        token: state.app.credentials.access_token,
        user_id: state.app.credentials.user_id,
        logged_in: state.app.credentials.logged_in,
        subscriptions: state.content.subscriptions
    }
}
let mapDispatchToProps = {
    onItemValueChanged,
    getSubscriptions
}
MeuPerfil = connect(mapStateToProps,mapDispatchToProps)(MeuPerfil);
export {MeuPerfil};