import React, { Component } from 'react';
import { Button } from 'reactstrap';
import { CardSlot } from './CardSlot';
import { connect } from 'react-redux';
import { StockCard } from './StockCard';
import { subscribeToActivity, unsubscribeToActivity } from '../actions/ContentActions';
class Home extends Component{
    render(){
        return(
            <div>
                {this.getCardSlot()}
                {this.props.children}
            </div>
        )
    }
    getCardSlot(){
        return(
            this.props.activities
            .reverse() // Make shure next reverse doesn't mess up with the cards order
            .reduce((resultArray, item, index) => { 
                const chunkIndex = Math.floor(index/2)

                if(!resultArray[chunkIndex]) {
                    resultArray[chunkIndex] = [] // start a new chunk
                }

                resultArray[chunkIndex].push(item)

                return resultArray
            }, [])
            .reverse()  //If there is a chunk with 1 card, this must be at the top
            .map((chunk, index)=>{
                return(
                    <CardSlot key={index}>
                    {
                        chunk.map((card,index)=>{
                            return(
                                <StockCard key={index} card={card}>
                                    <Button onClick={card.button_text==="Cancel"?
                                                        ()=>this.props.unsubscribeToActivity(this.props.user_id,card.id,this.props.token):
                                                        ()=>this.props.subscribeToActivity(this.props.user_id,card.id,this.props.token)}> 
                                    {card.button_text}
                                    </Button>
                                </StockCard>
                            )
                        }).reverse()    //Sort cards by id ascending from left to right
                    }
                    </CardSlot>
                )
            })
        );
    }
};
function mapStateToProps(state){
    return {
        activities: state.content.activities,
        user_id: state.app.credentials.user_id,
        token: state.app.credentials.access_token,
    }
}
let mapDispatchToProps = {
    subscribeToActivity,
    unsubscribeToActivity
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);