import React from 'react';
import { Container } from 'reactstrap';
import '../../css/App.css';
import { Content } from './Content';
import Header from './Header';
import { Footer } from './Footer';

export default function App (props){
  return (
    <div className="App">
      <Header />
        <Container className="Conteiner">
          <Content page={props.match.params.filter || "Home"} />
        </Container>
      <Footer />
    </div>
  );
}