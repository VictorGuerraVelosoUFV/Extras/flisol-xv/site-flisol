import React, { Component } from 'react';
import { FormGroup, Label, Input } from 'reactstrap';
import TextInputMask from 'react-masked-text';
import { connect } from "react-redux";
import { onItemValueChanged } from "../actions/FormActions";
function FormItem(props){
    return(
        <FormGroup>
            <Label for={ props.id }>{ props.label }</Label>
            { props.children }
        </FormGroup>
    )
}
class FormInputItem extends Component{
    onValueChange(event){
        this.props.onItemValueChanged(this.props.id, event.target.value);
    }
    render(){
        return(
            <FormItem id={ this.props.id } label={ this.props.label }>
                <Input { ...this.props } onChange={ this.onValueChange.bind(this) } value={this.props.form_inputs[this.props.id]} name={ this.props.id } id={ this.props.id } placeholder={ this.props.placeholder } />
            </FormItem>
        )
    }
}
function mapStateToProps(state){
    return {
        form_inputs: state.header.form_inputs,
    }
}
const mapDispatchToProps = {
    onItemValueChanged
}
const conn = connect(mapStateToProps,mapDispatchToProps);
FormInputItem = conn(FormInputItem);
class FormTextAreaItem extends Component{
    onValueChange(event){
        this.props.onItemValueChanged(this.props.id, event.target.value);
    }
    render(){
        return(
            <FormItem id={ this.props.id } label={ this.props.label }>
                <textarea onChange={ this.onValueChange.bind(this) } value={this.props.form_inputs[this.props.id]} className="form-control rounded-0" name={ this.props.id } id={ this.props.id } placeholder={ this.props.placeholder } />
            </FormItem>
        )
    }
}
FormTextAreaItem = conn(FormTextAreaItem);
class FormTextInputMaskItem extends Component{
    onValueChange(value){
        this.props.onItemValueChanged(this.props.id, value);
    }
    render(){
        return(
            <FormItem id={ this.props.id } label={ this.props.label }>
                <TextInputMask onChangeText={ this.onValueChange.bind(this) } value={this.props.form_inputs[this.props.id]} className="form-control" name={ this.props.id } id={ this.props.id } placeholder={ this.props.placeholder } kind={ this.props.type } />
            </FormItem>
        )
    }
}
FormTextInputMaskItem = conn(FormTextInputMaskItem);
class FormOptionItem extends Component{
    onValueChange(event){
        this.props.onItemValueChanged(this.props.id, event.target.checked);
    }
    render(){
        return(
            <FormGroup check>
                <Label check>
                    <Input onChange={ this.props.onChange || this.onValueChange.bind(this) } checked={this.props.form_inputs[this.props.id]} type={ this.props.option_type } id={ this.props.id } name={ this.props.name } />{' '}
                    { this.props.id }
                </Label>
            </FormGroup>
        )
    }
}
FormOptionItem = conn(FormOptionItem);
class FormRadioItem extends Component{
    onValueChange(){    //Override FormOptionItem's onValueChange function
        this.props.onItemValueChanged(this.props.name,this.props.id);
    }
    render(){
        return(
            <FormOptionItem option_type="radio" onChange={ this.onValueChange.bind(this) } { ...this.props } />
        )
    }
}
FormRadioItem = conn(FormRadioItem);
function FormCheckItem(props){
        return(
            <FormOptionItem option_type="checkbox" { ...props } />
        )
}
export { FormInputItem, FormTextAreaItem, FormTextInputMaskItem, FormRadioItem, FormCheckItem };