import React, { Component } from 'react';
import { Form, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { closeModal } from '../actions/HeaderActions';
import { submitFormData } from "../actions/FormActions";
import { connect } from 'react-redux';
import { FormInputItem, FormTextInputMaskItem } from './FormItem';
class StockModal extends Component{
  onSubmit(){
    if(this.props.modal_opened){
      switch(this.props.modal_type){
        case "Palestrante":{
          break;
        }
        case "Organizador":{
          break;          
        }
        case "Doador":{
          break;          
        }
        case "Representante de comunidade":{
          break;          
        }
        case "Cadastro":{
          let data = {
              cpf: this.props.form_inputs.CPF,
              name: this.props.form_inputs.name,
              email: this.props.form_inputs.email,
              rg: this.props.form_inputs.RG,
              username: this.props.form_inputs.email, //Authentication credential
              password: this.props.form_inputs.password
          }
          if(this.props.form_inputs.telefone){
            data = {...data, phone: this.props.form_inputs.telefone};
          }
          this.props.submitFormData("/accounts",data);
          break;
        }
        case "Login":{          
          let data = {
              username: this.props.form_inputs.email, //Authentication credential
              password: this.props.form_inputs.password
          }
          this.props.submitFormData("/accounts/login",data);
          break;
        }
        default:{
          console.log("TEM ALGO ERRADO AÍ, não existe esse modal");
        }
      }
    }
  }
    /*
    deprecatedonSubmit(){ 
      let modal_types = ["Palestrante","Organizador","Doador","Representante de comunidade"] //Gambiarra para adequar a API da athena q recebe numeros ao inves de string
      let horarios_possiveis = ["10h - 11h", "11h - 12h", "13h - 14h", "14h - 15h", "15h - 16h", "16h - 17h", "10h - 12h", "13h - 15h", "15h - 17h"];
      let tipos_atividade = ["Palestra","Workshop","Mesa Redonda"];
      let horarios = Object.keys(this.props.form_inputs).filter((key)=>horarios_possiveis.some((element)=>key === element && this.props.form_inputs[key] === true));
      let tipo_atividade = Object.keys(this.props.form_inputs).filter((key)=>tipos_atividade.some((element)=>key === element && this.props.form_inputs[key] === true));
      console.log(horarios);
      console.log(tipo_atividade);
      let body = {
        cpf: this.props.form_inputs.CPF,
        nome: this.props.form_inputs.name,
        email: this.props.form_inputs.email,
        telefone: this.props.form_inputs.telefone,
        rg: this.props.form_inputs.RG,
        titulo_atividade: this.props.form_inputs.atvdName,
        tipo_atividade: tipos_atividade.indexOf(this.props.form_inputs.tipo_atividade),
        descricao: this.pros.form_inputs.atvdDesc,
        materiais: horarios.toString(),
        software_necessario: this.props.form_inputs.atvdReq,
        tipo: this.props.modal_opened?modal_types.indexOf(this.props.modal_type):-1
    };
      axios.post("http://localhost:41062/www/insert_contribuinte.php", body)
          .then((value)=>console.log(value))
          .catch((error)=>console.log(error));
    }
    */
    
    render(){
        return (
          <Modal isOpen={this.props.modal_opened && this.props.opened} toggle={this.props.closeModal} className={this.props.className}>
            <ModalHeader toggle={this.props.closeModa}>{this.props.title}</ModalHeader>
            <ModalBody>
              <Form>
                  {this.props.default?(
                    <div>
                        <FormInputItem id="name" label="Nome Completo" placeholder="Escreva seu nome" />
                        <FormTextInputMaskItem id="telefone" label="Telefone" type="cel-phone" placeholder="Escreva seu telefone" />
                    </div>):<div></div>}
                <FormInputItem type="email" id="email" label="Email" placeholder="Escreva seu e-mail" />
                
                {this.props.children}
              </Form>
            </ModalBody>
            <ModalFooter>
              <Button type="submit" color="primary" onClick={this.onSubmit.bind(this)}>OK</Button>{' '}
              <Button color="secondary" onClick={this.props.closeModal}>Cancelar</Button>
            </ModalFooter>
          </Modal>
        );
    }
}

const mapDispatchToProps = {
    closeModal,
    submitFormData
};
function mapStateToProps(state){
    return {
      modal_type: state.header.modal_type,
      modal_opened: state.header.modal_opened,
      form_inputs: state.header.form_inputs
    };
};
StockModal = connect(mapStateToProps,mapDispatchToProps)(StockModal);
export { StockModal };