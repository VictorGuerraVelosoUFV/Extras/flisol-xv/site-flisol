import "../../css/StockCard.css";
import React from 'react';
import { Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle } from 'reactstrap';

export function StockCard(props){
    return (
        <Card {...props} className="Cartao">
            <CardImg top width="100%" src={ props.card.src } alt="Card image cap" />
            <CardBody>
                <CardTitle> {props.card.title} </CardTitle>
                <CardSubtitle> {props.card.subtitle} </CardSubtitle>
                <CardText> {props.card.text} </CardText>
                {props.children}
            </CardBody>
        </Card>
    )
}