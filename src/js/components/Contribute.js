import React, { Component } from 'react';
import { FormGroup, Label } from 'reactstrap';
import { StockModal } from './StockModal';
import { FormCheckItem, FormRadioItem, FormInputItem, FormTextAreaItem, FormTextInputMaskItem } from './FormItem';
import { connect } from "react-redux";


//http -f POST 192.168.0.101/flisol/insert_contribuinte.php cpf="12062730624" nome="Victor" email="victorgvbh@gmail.com" telefone="34916232" rg="MG119155686" tipo_atividade=0 descricao="Ser o maior programador php do mundo"
// materiais="um pc e muito café" software_necessario="ide codeblocks" tipo=0
function contributePalestrante(){
  return (
      <div>
        <FormInputItem id="RG" label="RG" placeholder="Escreva seu RG" />
        <FormTextInputMaskItem id="CPF" label="CPF" type="cpf" placeholder="Escreva seu CPF" />
        <FormInputItem id="atvdName" label="Título da atividade" placeholder="Escreva o título de seu palestra/workshop" />
        <FormGroup tag="fieldset">
          <Label>Tipo de atividade:</Label>
          <FormRadioItem name="tipo_atividade" id="Palestra" />
          <FormRadioItem name="tipo_atividade" id="Workshop" />
          <FormRadioItem name="tipo_atividade" id="Mesa Redonda" />
        </FormGroup>
        <FormTextAreaItem id="atvdDesc" label="Descrição" placeholder="Faça uma breve descrição da sua atividade" />
        <FormTextAreaItem id="atvdReq" label="Softwares necessários" placeholder="Liste quais softwares devem estar instalados nos computadores do laboratório para a sua atividade" />
        <FormGroup tag="fieldset">
          <legend>Horários de preferência:</legend>
          <FormCheckItem id="10h - 11h" />
          <FormCheckItem id="11h - 12h" />
          <FormCheckItem id="13h - 14h" />
          <FormCheckItem id="14h - 15h" />
          <FormCheckItem id="15h - 16h" />
          <FormCheckItem id="16h - 17h" />
          <FormCheckItem id="10h - 12h" />
          <FormCheckItem id="13h - 15h" />
          <FormCheckItem id="15h - 17h" />
        </FormGroup>
      </div>
    );
}
function contributeOrganizador(){
  return (
      <div>
        <FormTextInputMaskItem id="CPF" label="CPF" type="cpf" placeholder="Escreva seu CPF" />
        <FormTextAreaItem id="orgMethod" label="Forma de contribuição" placeholder="Descreva como pretende ajudar na organização" />
      </div>
  )
}
function contributeDoador(){
  return (
      <div>
        <FormTextAreaItem id="donationMethod" label="Forma de doação" placeholder="Descreva o que pretende doar para o evento" />
        <a href="https://drive.google.com/file/d/1JMncKHjg4oLe1n2tzYHCcLT-FFZAlIgr/view?usp=sharing" target="_blank" rel="noopener noreferrer">Lista de materiais</a>
      </div>
    );
}
function contributeComunidade(){
  return <FormInputItem id="commName" label="Nome da comunidade" placeholder="Escreva o nome de sua comunidade" />
}
class Contribute extends Component {
  constructor(props) {
    super(props);
    this.title = "";
  }
  
  render() {
    this.title = this.props.modal_type;
    switch (this.props.modal_type) {
      case "Palestrante":
        this.extra = contributePalestrante();
        break;
      case "Organizador":
        this.extra = contributeOrganizador();
        break;
      case "Doador":
        this.extra = contributeDoador(); 
        break;
      case "Representante de comunidade":
        this.extra = contributeComunidade(); 
        break;
      default:
        return [];
    }
    return (
      <StockModal default title={this.title} opened={this.props.modal_type === this.title}>
        { this.extra }
      </StockModal>
    );
  }
}

function mapStateToProps(state){
  return {
      modal_opened: state.header.modal_opened,
      modal_type: state.header.modal_type
  }
}

Contribute = connect(mapStateToProps)(Contribute);
export { Contribute };