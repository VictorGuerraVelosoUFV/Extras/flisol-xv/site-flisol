import React, { Component } from 'react';
import { StockModal } from './StockModal';
import { FormInputItem } from './FormItem';
import { connect } from 'react-redux';
class Login extends Component{
  render(){
    return (
      <StockModal title="Login" opened={this.props.modal_type === "Login"}>
        <FormInputItem type="password" id="password" label="Senha" placeholder="Escreva sua senha" />
      </StockModal>
    );
  }
};
function mapStateToProps(state){
    return {
        modal_type: state.header.modal_type
    }
}
Login = connect(mapStateToProps)(Login);
export { Login };