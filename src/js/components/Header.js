import React, { Component } from 'react';
import { Collapse, DropdownToggle, DropdownMenu, DropdownItem, Nav, Navbar, NavbarBrand, NavItem, NavLink, NavbarToggler, UncontrolledDropdown} from 'reactstrap';
import '../../css/Header.css';
import { Login } from './Login';
import { Register } from './Register';
import { Contribute } from './Contribute';
import { isString } from 'util';
import logo_flisol from '../../assets/logo_flisol.svg';
import { Link } from 'react-router-dom'
import { openModal, setModalType, userLogout } from '../actions/HeaderActions';
import { connect } from 'react-redux';
class Header extends Component{
    constructor(props){
      super(props);
      this.state = {
        location: "",
        isOpen: true,               //Colapsado em mobile?
      };
      this.toggle = this.toggle.bind(this);
      this.setContribution = this.setContribution.bind(this);
      this.callModal = this.callModal.bind(this);
    }
    toggle(){
        this.setState((prevState) => ({
            isOpen: !prevState.isOpen,
        }));
    }
    setContribution(param){
        if(isString(param)){
            this.props.setModalType(param);
            this.props.openModal();
        }
    }
    callModal(title){
        this.props.setModalType(title);
        this.props.openModal();
    }
    getLocation(){
        if(this.state.location === ""){
            navigator.geolocation.getCurrentPosition(
                (position)=>this.setState({location:position.coords.latitude + "," + position.coords.longitude}),
                (error)=>console.log("Error"),
                {
                    enableHighAccuracy: true,
                    timeout : 5000
                }
            );
        }
        return "https://www.google.com/maps/dir/"+this.state.location+"/cefet+mg+campus+2+google+maps/@-19.909922,-44.0107048,13z/data=!3m1!4b1!4m9!4m8!1m1!4e1!1m5!1m1!1s0xa6965ceade4c53:0x4980bb6236578f78!2m2!1d-43.9992624!2d-19.9392315";
    }
    render(){
        console.log(this.props);
        return (
            <Navbar light expand="md" className="header">
            <NavbarBrand className="d-md-none col-4 text-left pointer" tag={Link} to="/Home" >FLISoL MG</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse className="justify-content-between align-items-center w-100" isOpen={this.state.isOpen} navbar>
                <Nav className="d-none d-md-block col-4 text-center" navbar>
                <NavItem tag={Link} to="/Home" ><img className="w-75 max-w-100 pointer" alt="Logo" src={logo_flisol}></img></NavItem>
                </Nav>
                <Nav className="ml-auto" navbar>
                <NavItem>
                    <NavLink tag={Link} to="/About">Sobre</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href={this.getLocation()} target="_blank">Como chegar?</NavLink>
                </NavItem>
                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                    Atrações
                    </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem tag={Link} to="/InstallFest" >
                            #InstallFest
                        </DropdownItem>
                        <DropdownItem tag={Link} to="/Palestras">
                            #Palestras
                        </DropdownItem>
                        <DropdownItem tag={Link} to="/Workshops">
                            #Workshops
                        </DropdownItem>
                        <DropdownItem tag={Link} to="/EspacoKids">
                            #EspaçoKids
                        </DropdownItem>
                        <DropdownItem tag={Link} to="/MaratonaDeProgramacao">
                            #MaratonaDeProgramação
                        </DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                    Contribue!
                    </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem onClick={()=>(this.callModal("Palestrante"))}>
                            Palestrante
                        </DropdownItem>
                        <DropdownItem onClick={()=>(this.callModal("Organizador"))}>
                            Organizador
                        </DropdownItem>
                        <DropdownItem onClick={()=>(this.callModal("Doador"))}>
                            Doador
                        </DropdownItem>
                        <DropdownItem onClick={()=>(this.callModal("Representante de comunidade"))}>
                            Representante de comunidade
                        </DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
                {this.props.logged_in?
                    (
                        <NavItem>
                            <NavLink tag={Link} to="/MeuPerfil">Meu Perfil</NavLink>
                        </NavItem>
                    ):
                    (
                        <NavItem>
                            <NavLink className="pointer" onClick={()=>this.callModal("Cadastro")}>Cadastrar</NavLink>
                        </NavItem>
                    )
                }
                {this.props.logged_in?
                    (
                        <NavItem>
                            <NavLink className="pointer" onClick={()=>this.props.userLogout(this.props.token)}>Sair</NavLink>
                        </NavItem>
                    ):
                    (
                        <NavItem>
                            <NavLink className="pointer" onClick={()=>this.callModal("Login")}>Entrar</NavLink>
                        </NavItem>
                    )
                }
                <Login />
                <Register />
                <Contribute />
                
                </Nav>
            </Collapse>
            </Navbar>
        )
    }
};
function mapStateToProps(state){
    return {
        modal_opened: state.header.modal_opened,
        modal_type: state.header.modal_type,
        logged_in: state.app.credentials.logged_in,
        token: state.app.credentials.access_token
    }
}

const mapDispatchToProps = {
    openModal,
    setModalType: (type) => setModalType(type),
    userLogout
}
export default connect(mapStateToProps,mapDispatchToProps)(Header);