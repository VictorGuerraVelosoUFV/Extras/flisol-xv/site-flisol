import React from 'react';
import Home from './Home';
import { About } from './About';
import { Attraction } from './Attraction';
import { Alert } from 'reactstrap';
import { InstallFest } from "./InstallFest";
import { Palestras } from "./Palestras";
import { MaratonaDeProgramacao } from './MaratonadeProg';
import { Workshops } from './Workshops';
import { EspacoKids } from './EspacoKids';
import { MeuPerfil } from './MeuPerfil';
export function Content (props) {
    switch(props.page){
        case "Home":
            return (<Home />);
        case "About":
            return (<About />);
        case "Attraction":
            return (<Attraction />);
        case "InstallFest":
            return (<InstallFest />);
        case "Palestras":
            return (<Palestras />);
        case "Workshops":
            return (<Workshops />);
        case "EspacoKids":
            return (<EspacoKids />);
        case "MaratonaDeProgramacao":
            return (<MaratonaDeProgramacao />);
        case "MeuPerfil":
            return (<MeuPerfil />);
        default:
            return (
                <Alert color="danger">
                    Erro inesperado! Não foi possível identificar a pagina requisitada.
                </Alert>
            );
    }
};