import { combineReducers } from 'redux';
import Content from './ContentReducer';
import Header from './HeaderReducer';
import App from './AppReducer';

export default combineReducers({
    content: Content,
    header: Header,
    app: App
});