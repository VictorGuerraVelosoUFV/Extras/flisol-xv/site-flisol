let initialState = {
    modal_type: "",
    modal_opened: false,
    form_inputs: { shirt_size: "M" },
    form_outputs: {
        data: {},
        path: []
    }
};
export default function reducer(state=initialState, action){
        switch(action.type){
            case "SET_MODAL_STATE": {
                return {
                    ...state,
                    modal_opened: action.payload.opened
                }
            }
            case "SET_MODAL_TYPE": {
                return {
                    ...state,
                    modal_type: action.payload.type
                }
            }
            case "FORM_ITEM_VALUE_CHANGED": {
                return {
                    ...state,
                    form_inputs: {...(state.form_inputs), [action.payload.id]: action.payload.change},
                }
            }
            case "SEND_DATA_SUCCEEDED": {
                return {
                    ...state,
                    modal_opened: false,
                    form_outputs: {
                        data: {
                            [action.payload.operation.path]: action.payload.data
                        },
                        path: [...state.form_outputs.path, action.payload.operation.path]
                    }
                };
            }
            case "SEND_DATA_FAILED": {
                return {
                    ...state,
                    form_outputs: {
                        error: action.payload.error,
                        path: action.payload.operation.path
                    }
                };
            }
            default: {
                return state;
            }
        }
    }