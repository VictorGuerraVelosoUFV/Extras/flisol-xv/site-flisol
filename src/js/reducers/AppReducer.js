var FLISOL_TOKEN = "@flisol-token";
var FLISOL_USER_ID = "@flisol-user-id";
let initialState = {
    credentials: {
        logged_in: !!localStorage.getItem(FLISOL_TOKEN),
        access_token: localStorage.getItem(FLISOL_TOKEN),
        user_id: localStorage.getItem(FLISOL_USER_ID)
    },
}
export default function reducer(state = initialState, action){
        switch(action.type){
            case "LOGOUT": {
                console.log("OI");
                return state;
            }
            case "LOGOUT_FAILED": {
                console.log(action.payload);
                return state;
            }
            case "LOGOUT_SUCCEEDED": {
                console.log("SUC")
                localStorage.removeItem(FLISOL_TOKEN);
                localStorage.removeItem(FLISOL_USER_ID);
                return {
                    ...state,
                    credentials: {
                        logged_in: false,
                        access_token: "",
                        user_id: "",
                    }
                }
            }
            case "SEND_DATA_SUCCEEDED": {
                if(action.payload.operation.path === "/accounts/login"){
                    localStorage.setItem(FLISOL_TOKEN,action.payload.data.data.id);
                    localStorage.setItem(FLISOL_USER_ID,action.payload.data.data.userId);
                    return {
                        ...state,
                        credentials: {
                            logged_in: true,
                            access_token: action.payload.data.data.id,
                            user_id: action.payload.data.data.userId,
                        }
                    }
                }
                return state;
            }
            default: {
                return state;
            }
        }
}