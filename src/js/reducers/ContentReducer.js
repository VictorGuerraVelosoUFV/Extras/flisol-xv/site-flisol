import { CardModel } from "../models/CardModel";
import card1 from "../../assets/card1.png";
let initialState = {
    activities:   [new CardModel(card1, "Criação de Documentos com LaTeX", "10h-12h - Laboratório 201", "Um workshop introdutório ao Latex, linguagem de formatação para o programa de diagramação de textos Tex, utilizado amplamente na produção de textos matemáticos e científicos além de relatórios, cartas e documentos. Após a introdução no workshop é esperado que o participante tenha condições de seguir sozinho o seu aprendizado no uso de Latex.", 1, "Inscreva-se"),
                new CardModel(card1, "Title 2", "Subtile 2", "content 2", 2),
                new CardModel(card1, "Title 3", "Subtile 3", "content 3", 3),
                new CardModel(card1, "Title 4", "Subtile 4", "content 4", 4),
                new CardModel(card1, "Title 5", "Subtile 5", "content 5", 5),
                new CardModel(card1, "Title 6", "Subtile 6", "content 6", 6),
                new CardModel(card1, "Title 7", "Subtile 7", "content 7", 7)],
    subscriptions: []
}
export default function reducer(state = initialState, action){
        switch(action.type){
            case "GET_SUBS_SUCCEEDED": {
                return {
                    ...state,
                    subscriptions: [...state.subscriptions, ...(action.payload.data.data.map((card)=>new CardModel(card.img_url,card.title,card.subtitle,card.description,card.id)))]
                }
            }
            case "SET_SUBS_SUCCEEDED": {
                const clicked_card = state.activities.find((value)=>value.id === action.payload.data.data.activityId);
                return {
                    ...state,
                    activities: [
                        ...state.activities.filter((value)=>value.id !== action.payload.data.data.activityId),
                        new CardModel(clicked_card.src, clicked_card.title, clicked_card.subtitle, clicked_card.text, clicked_card.id, "Cancel")
                    ].slice().sort((a,b)=>a.id>b.id)
                };
            }
            case "EDIT_ACTIVITIES": {
                return {
                    ...state,
                    activities: [...(state.activities), ...(action.payload.activities)],
                }
            }
            default: {
                return state;
            }
        }
    }