import Axios from "axios";

export function onItemValueChanged(id, change){
    return{
        type: "FORM_ITEM_VALUE_CHANGED",
        payload: {
            id,
            change
        }
    }
}
let server = Axios.create({baseURL:"http://localhost:3000/api"});

export function submitFormData(path,data){
    return (dispatch) => {
        dispatch(sendFormData());
        server.post(path,data)
        .then((value)=>dispatch(sendFormDataSuccess(path,value)))
        .catch((error)=>dispatch(sendFormDataError(path,error)));
    }
}

function sendFormData(){
    return{
        type: "SEND_DATA"
    };
}

function sendFormDataSuccess(path,data){
    return{
        type: "SEND_DATA_SUCCEEDED",
        payload: {
            operation: {
                path
            },
            data
        }
        
    };
}

function sendFormDataError(path,error){
    return{
        type: "SEND_DATA_FAILED",
        payload: {
            operation: {
                path
            },
            error
        }
    };
}