import Axios from "axios";
let server = Axios.create({ baseURL: "http://localhost:3000/api" });

export function getSubscriptions(user_id, token) {
    return (dispatch) => {
        dispatch(getUserSubscriptions());
        server.get(`/accounts/${user_id}/subscriptions?access_token=${token}`)
            .then((value) => dispatch(getUserSubscriptionsSuccess(value)))
            .catch((error) => dispatch(getUserSubscriptionsError(error)));
    }
}

function getUserSubscriptions() {
    return {
        type: "GET_SUBS"
    };
}
function getUserSubscriptionsSuccess(data) {
    return {
        type: "GET_SUBS_SUCCEEDED",
        payload: {
            data
        }
    };
}
function getUserSubscriptionsError(error) {
    return {
        type: "GET_SUBS_FAILED",
        payload: {
            error
        }
    };
}

export function subscribeToActivity(user_id, activity_id, token) {
    return (dispatch) => {
        dispatch(subscribeUserToActivity());
        server.put(`/accounts/${user_id}/subscriptions/rel/${activity_id}?access_token=${token}`)
            .then((value) => dispatch(subscribeUserToActivitySuccess(value)))
            .catch((error) => dispatch(subscribeUserToActivityError(error)));
    }
}
function subscribeUserToActivity() {
    return {
        type: "SET_SUBS"
    };
}
function subscribeUserToActivitySuccess(data) {
    return {
        type: "SET_SUBS_SUCCEEDED",
        payload: {
            data
        }
    };
}
function subscribeUserToActivityError(error) {
    return {
        type: "SET_SUBS_FAILED",
        payload: {
            error
        }
    };
}
export function unsubscribeToActivity(user_id, activity_id, token) {
    return (dispatch) => {
        dispatch(unsubscribeUserToActivity());
        server.delete(`/accounts/${user_id}/subscriptions/rel/${activity_id}?access_token=${token}`)
            .then((value) => dispatch(unsubscribeUserToActivitySuccess(value)))
            .catch((error) => dispatch(unsubscribeUserToActivityError(error)));
    }
}
function unsubscribeUserToActivity() {
    return {
        type: "UNSET_SUBS"
    };
}
function unsubscribeUserToActivitySuccess(data) {
    return {
        type: "UNSET_SUBS_SUCCEEDED",
        payload: {
            data
        }
    };
}
function unsubscribeUserToActivityError(error) {
    return {
        type: "UNSET_SUBS_FAILED",
        payload: {
            error
        }
    };
}
