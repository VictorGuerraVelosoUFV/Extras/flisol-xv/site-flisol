import Axios from "axios";

let server = Axios.create({baseURL:"http://localhost:3000/api"});

export function userLogout(token){
    return (dispatch) => {
        dispatch(logout());
        server.post(`/accounts/logout?access_token=${token}`)
        .then((value)=>dispatch(logoutSuccess(value)))
        .catch((error)=>dispatch(logoutError(error)));
    }
}

function logout(){
    return{
        type: "LOGOUT",
    }
}

function logoutSuccess(data){
    return{
        type: "LOGOUT_SUCCEEDED",
        payload: {
            data
        }
    }
}

function logoutError(error){
    return{
        type: "LOGOUT_FAILED",
        payload: {
            error
        }
    }
}

export function editNews(news){
    return{
        type: "EDIT_NEWS",
        payload: {
            news
        }
    }
}

export function setModalType(type){
    return{
        type: "SET_MODAL_TYPE",
        payload: {
            type
        }
    }
}

export function openModal(){
    return{
        type: "SET_MODAL_STATE",
        payload: {
            opened: true
        }
    };
}

export function closeModal(){
    return{
        type: "SET_MODAL_STATE",
        payload: {
            opened: false
        }
    };
}