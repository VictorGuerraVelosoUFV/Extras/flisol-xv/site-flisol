export class CardModel{
    constructor(src, title, subtitle, text, id=-1, button_text="Inscreva-se"){
        this.src = src;
        this.title = title;
        this.subtitle = subtitle;
        this.text = text;
        this.id = id;
        this.button_text = button_text;
    }
};